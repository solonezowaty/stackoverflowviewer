package com.example.solonez.stackoverflowviewer.main;

import android.content.Intent;
import android.os.Bundle;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import com.example.solonez.stackoverflowviewer.R;
import com.example.solonez.stackoverflowviewer.ui.ActionBarActivity;
import butterknife.BindView;
import butterknife.ButterKnife;

public class WebViewActivity extends ActionBarActivity {

    @BindView(R.id.web_view_browser) WebView webView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.web_view_browser);
        ButterKnife.bind(this);

        Intent intent = getIntent();
        String link = intent.getStringExtra("link");
        webView.setWebViewClient(new WebViewClient());
        WebSettings webSettings = webView.getSettings();
        webSettings.setJavaScriptEnabled(true);
        webView.loadUrl(link);
    }

    @Override
    protected boolean getBackButtonEnabled() {
        return true;
    }

    @Override
    protected String getActionBarTitle() {
        return getString(R.string.question_browser);
    }
}
