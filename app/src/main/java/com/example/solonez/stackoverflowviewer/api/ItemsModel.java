package com.example.solonez.stackoverflowviewer.api;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Jakub Solecki on 2018-06-14.
 */
public class ItemsModel {

    @Expose
    private boolean is_answered;
    @Expose
    @SerializedName("view_count")
    private int viewCount;
    @Expose
    @SerializedName("accepted_answer_id")
    private int acceptedAnswerId;
    @Expose
    @SerializedName("answer_count")
    private int answerCount;
    @Expose
    private int score;
    @Expose
    @SerializedName("last_activity_date")
    private long lastActivityDate;
    @Expose
    @SerializedName("creation_date")
    private long creationDate;
    @Expose
    @SerializedName("last_edit_date")
    private long lastEditDate;
    @Expose
    @SerializedName("question_id")
    private int questionId;
    @Expose
    private String link;
    @Expose
    private String title;

    public boolean isIs_answered() {
        return is_answered;
    }

    public void setIs_answered(boolean is_answered) {
        this.is_answered = is_answered;
    }

    public int getViewCount() {
        return viewCount;
    }

    public void setViewCount(int viewCount) {
        this.viewCount = viewCount;
    }

    public int getAcceptedAnswerId() {
        return acceptedAnswerId;
    }

    public void setAcceptedAnswerId(int acceptedAnswerId) {
        this.acceptedAnswerId = acceptedAnswerId;
    }

    public int getAnswerCount() {
        return answerCount;
    }

    public void setAnswerCount(int answerCount) {
        this.answerCount = answerCount;
    }

    public int getScore() {
        return score;
    }

    public void setScore(int score) {
        this.score = score;
    }

    public long getLastActivityDate() {
        return lastActivityDate;
    }

    public void setLastActivityDate(long lastActivityDate) {
        this.lastActivityDate = lastActivityDate;
    }

    public long getCreationDate() {
        return creationDate;
    }

    public void setCreationDate(long creationDate) {
        this.creationDate = creationDate;
    }

    public long getLastEditDate() {
        return lastEditDate;
    }

    public void setLastEditDate(long lastEditDate) {
        this.lastEditDate = lastEditDate;
    }

    public int getQuestionId() {
        return questionId;
    }

    public void setQuestionId(int questionId) {
        this.questionId = questionId;
    }

    public String getLink() {
        return link;
    }

    public void setLink(String link) {
        this.link = link;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }
}
