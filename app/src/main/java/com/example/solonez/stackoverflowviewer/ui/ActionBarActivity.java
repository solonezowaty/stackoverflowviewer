package com.example.solonez.stackoverflowviewer.ui;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.MenuItem;

/**
 * Created by Jakub Solecki on 2018-06-13.
 */
public abstract class ActionBarActivity extends AppCompatActivity {

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getSupportActionBar().setTitle(getActionBarTitle());
        getSupportActionBar().setDisplayHomeAsUpEnabled(getBackButtonEnabled());
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()){
            case android.R.id.home:{
                finish();
                break;
            }
        }
        return true;
    }

    protected abstract boolean getBackButtonEnabled();

    protected abstract String getActionBarTitle();
}
