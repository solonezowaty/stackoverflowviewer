package com.example.solonez.stackoverflowviewer.ui;

import android.animation.Animator;
import android.view.View;
import android.view.ViewPropertyAnimator;

/**
 * Created by Jakub Solecki on 2018-06-11.
 */
public class Animations {

    public static void fadeInAnimation(final View view){
        fadeInAnimation(view, new Animator.AnimatorListener() {
            @Override
            public void onAnimationStart(Animator animation) {
                view.setVisibility(View.VISIBLE);
            }

            @Override
            public void onAnimationEnd(Animator animation) {

            }

            @Override
            public void onAnimationCancel(Animator animation) {

            }

            @Override
            public void onAnimationRepeat(Animator animation) {

            }
        });
    }

    public static void fadeInAnimation(View view, Animator.AnimatorListener animationListener){
        ViewPropertyAnimator animator = view.animate();
        if (animationListener != null)
            animator.setListener(animationListener);
            animator
                    .setDuration(2000)
                    .alpha(1.0f)
                    .start();
    }
}
