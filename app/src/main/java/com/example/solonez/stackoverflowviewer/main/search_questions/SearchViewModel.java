package com.example.solonez.stackoverflowviewer.main.search_questions;

import android.arch.lifecycle.MutableLiveData;
import android.arch.lifecycle.ViewModel;
import android.util.Log;

import com.example.solonez.stackoverflowviewer.api.ApiClient;
import com.example.solonez.stackoverflowviewer.api.ApiInterface;
import com.example.solonez.stackoverflowviewer.api.ItemsModel;
import com.example.solonez.stackoverflowviewer.api.QuestionsModel;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by Jakub Solecki on 15.06.2018.
 */

public class SearchViewModel extends ViewModel{

    private MutableLiveData<List<ItemsModel>> itemsQuestionsList;
    private String queryString;

    public String getQueryString() {
        return queryString;
    }

    public void setQueryString(String queryString) {
        this.queryString = queryString;
    }

    public MutableLiveData<List<ItemsModel>> getItemsQuestionsList() {
        if (itemsQuestionsList == null){
            itemsQuestionsList = new MutableLiveData<>();
            itemsQuestionsList.setValue(new ArrayList<ItemsModel>());
        }
        return itemsQuestionsList;
    }

    public void getItemsQuestions(){
        ApiInterface apiInterface = ApiClient.getRetrofit().create(ApiInterface.class);
        Call<QuestionsModel> call = apiInterface.getSearchQuestions("desc", "activity", getQueryString(), "stackoverflow");
        call.enqueue(new Callback<QuestionsModel>() {
            @Override
            public void onResponse(Call<QuestionsModel> call, Response<QuestionsModel> response) {
                if (response.body() != null && response.isSuccessful()){
                    if (response.code() == 200){
                        itemsQuestionsList.setValue(response.body().getItemsModel());
                    }else {
                        Log.e("Error: ", response.code() + " " + response.message());
                    }
                }
            }

            @Override
            public void onFailure(Call<QuestionsModel> call, Throwable t) {
                t.printStackTrace();
            }
        });
    }
}
