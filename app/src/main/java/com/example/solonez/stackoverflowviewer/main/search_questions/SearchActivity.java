package com.example.solonez.stackoverflowviewer.main.search_questions;

import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;
import com.example.solonez.stackoverflowviewer.api.ItemsModel;
import com.example.solonez.stackoverflowviewer.ui.ActionBarActivity;
import com.example.solonez.stackoverflowviewer.R;
import java.util.List;
import butterknife.BindView;
import butterknife.ButterKnife;

public class SearchActivity extends ActionBarActivity {

    @BindView(R.id.search_recycler_view) RecyclerView recyclerView;
    @BindView(R.id.search_icon) ImageView searchIcon;
    @BindView(R.id.search_edit_frame) EditText searchEditText;
    private SearchViewModel searchViewModel;
    private RecyclerView.Adapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search);
        ButterKnife.bind(this);

        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(SearchActivity.this);
        recyclerView.setLayoutManager(layoutManager);
        searchViewModel = ViewModelProviders.of(SearchActivity.this).get(SearchViewModel.class);
        adapter = new SearchAdapter(searchViewModel.getItemsQuestionsList());
        recyclerView.setAdapter(adapter);
        initializeObserver();
        setOnClickListeners();
    }

    private void setOnClickListeners(){
        searchIcon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (searchEditText.getText().toString().matches("")){
                    Toast.makeText(SearchActivity.this, "Wprowadź czego dokładnie szukasz.", Toast.LENGTH_SHORT).show();
                } else {
                    searchViewModel.setQueryString(searchEditText.getText().toString());
                    searchViewModel.getItemsQuestions();
                }
            }
        });
    }

    @Override
    protected boolean getBackButtonEnabled() {
        return true;
    }

    @Override
    protected String getActionBarTitle() {
        return getString(R.string.search);
    }

    private void initializeObserver(){
        final Observer<List<ItemsModel>> questions = new Observer<List<ItemsModel>>(){

            @Override
            public void onChanged(@Nullable List<ItemsModel> itemsModelList) {
                adapter.notifyDataSetChanged();
            }
        };
        searchViewModel.getItemsQuestionsList().observe(this, questions);
    }
}
