package com.example.solonez.stackoverflowviewer.api;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by Jakub Solecki on 2018-06-14.
 */
public class QuestionsModel {

    @Expose
    @SerializedName("items")
    private List<ItemsModel> itemsModel;

    public List<ItemsModel> getItemsModel() {
        return itemsModel;
    }

    public void setItemsModel(List<ItemsModel> itemsModel) {
        this.itemsModel = itemsModel;
    }
}
