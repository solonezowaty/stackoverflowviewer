package com.example.solonez.stackoverflowviewer;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.RelativeLayout;
import com.example.solonez.stackoverflowviewer.main.hottest_questions.HottestQuestionsActivity;
import com.example.solonez.stackoverflowviewer.main.search_questions.SearchActivity;
import com.example.solonez.stackoverflowviewer.main.week_questions.WeekQuestionsActivity;
import com.example.solonez.stackoverflowviewer.ui.Animations;
import butterknife.BindView;
import butterknife.ButterKnife;

public class StartActivity extends AppCompatActivity {

    @BindView(R.id.start_activity_layout) RelativeLayout startActivityLayout;
    @BindView(R.id.search_for_questions_button) Button searchForQuestion;
    @BindView(R.id.hottest_questions_button) Button hottestQuestionsButton;
    @BindView(R.id.week_questions_button) Button weekQuestionsbutton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_start);

        ButterKnife.bind(this);
        Animations.fadeInAnimation(startActivityLayout);
        setOnClickListeners();
    }

    private void setOnClickListeners(){
        searchForQuestion.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startChoosenIntent(SearchActivity.class);
            }
        });

        hottestQuestionsButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startChoosenIntent(HottestQuestionsActivity.class);
            }
        });

        weekQuestionsbutton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startChoosenIntent(WeekQuestionsActivity.class);
            }
        });
    }

    private void startChoosenIntent(Class<?> choosenActivity){
        Intent intent = new Intent(StartActivity.this, choosenActivity);
        startActivity(intent);
    }
}
