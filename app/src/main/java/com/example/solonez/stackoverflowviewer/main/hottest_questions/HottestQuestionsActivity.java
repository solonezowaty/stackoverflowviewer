package com.example.solonez.stackoverflowviewer.main.hottest_questions;

import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import com.example.solonez.stackoverflowviewer.api.ItemsModel;
import com.example.solonez.stackoverflowviewer.ui.ActionBarActivity;
import com.example.solonez.stackoverflowviewer.R;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class HottestQuestionsActivity extends ActionBarActivity {

    @BindView(R.id.hottest_recycler_view) RecyclerView recyclerView;
    private RecyclerView.Adapter adapter;
    private HottestQuestionsViewModel hottestQuestionsViewModel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_hottest_questions);
        ButterKnife.bind(this);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(HottestQuestionsActivity.this);
        recyclerView.setLayoutManager(layoutManager);
        hottestQuestionsViewModel = ViewModelProviders.of(this).get(HottestQuestionsViewModel.class);
        hottestQuestionsViewModel.getItemsQuestions();
        adapter = new HottestQuestionsAdapter(hottestQuestionsViewModel.getItemsQuestionsList());
        recyclerView.setAdapter(adapter);
        initializeObserver();
    }

    @Override
    protected boolean getBackButtonEnabled() {
        return true;
    }

    @Override
    protected String getActionBarTitle() {
        return getString(R.string.hottest_questions);
    }

    private void initializeObserver(){
        final Observer<List<ItemsModel>> questions = new Observer<List<ItemsModel>>(){

            @Override
            public void onChanged(@Nullable List<ItemsModel> itemsModelList) {
                adapter.notifyDataSetChanged();
            }
        };
        hottestQuestionsViewModel.getItemsQuestionsList().observe(this, questions);
    }
}
