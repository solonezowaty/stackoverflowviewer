package com.example.solonez.stackoverflowviewer.main.week_questions;

import android.arch.lifecycle.MutableLiveData;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.example.solonez.stackoverflowviewer.R;
import com.example.solonez.stackoverflowviewer.api.ItemsModel;
import com.example.solonez.stackoverflowviewer.main.WebViewActivity;
import java.util.Calendar;
import java.util.List;
import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Jakub Solecki on 15.06.2018.
 */

public class WeekQuestionsAdapter extends RecyclerView.Adapter<WeekQuestionsAdapter.ViewHolder>{

    private MutableLiveData<List<ItemsModel>> itemsModelList;
    private ItemsModel itemsModel;

    public WeekQuestionsAdapter (MutableLiveData<List<ItemsModel>> itemsModelList){
        this.itemsModelList = itemsModelList;
    }


    public static class ViewHolder extends RecyclerView.ViewHolder{
        @BindView(R.id.question_title) TextView questionTitle;
        @BindView(R.id.creation_date) TextView creationDate;
        @BindView(R.id.hits) TextView hits;
        @BindView(R.id.items_layout) RelativeLayout questionItem;

        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

    @Override
    public WeekQuestionsAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.recycler_item, parent, false);
        return new WeekQuestionsAdapter.ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        itemsModel = itemsModelList.getValue().get(position);
        final String link = itemsModel.getLink();

        Calendar cl = Calendar.getInstance();
        cl.setTimeInMillis(itemsModel.getCreationDate() * 1000);
        String date = String.format("%02d-%02d-%02d %02d:%02d", cl.get(Calendar.DAY_OF_MONTH), cl.get(Calendar.MONTH) +1, cl.get(Calendar.YEAR), cl.get(Calendar.HOUR_OF_DAY), cl.get(Calendar.MINUTE));

        holder.questionTitle.setText(itemsModel.getTitle());
        holder.creationDate.setText(date);
        holder.hits.setText(String.valueOf(itemsModel.getViewCount()));
        holder.questionItem.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent(v.getContext(), WebViewActivity.class);
                intent.putExtra("link", link);
                v.getContext().startActivity(intent);
            }
        });
    }

    @Override
    public int getItemCount() {
        return itemsModelList.getValue().size();
    }
}
