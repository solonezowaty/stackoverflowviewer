package com.example.solonez.stackoverflowviewer.main.week_questions;

import android.arch.lifecycle.MutableLiveData;
import android.arch.lifecycle.ViewModel;
import android.util.Log;
import com.example.solonez.stackoverflowviewer.api.ApiClient;
import com.example.solonez.stackoverflowviewer.api.ApiInterface;
import com.example.solonez.stackoverflowviewer.api.ItemsModel;
import com.example.solonez.stackoverflowviewer.api.QuestionsModel;
import java.util.ArrayList;
import java.util.List;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by Jakub Solecki on 15.06.2018.
 */

public class WeekQuestionsViewModel extends ViewModel {

    private MutableLiveData<List<ItemsModel>> weekItemsQuestionsList;

    public MutableLiveData<List<ItemsModel>> getWeekItemsQuestionsList() {
        if (weekItemsQuestionsList == null){
            weekItemsQuestionsList = new MutableLiveData<>();
            weekItemsQuestionsList.setValue(new ArrayList<ItemsModel>());
        }
        return weekItemsQuestionsList;
    }

    public void getWeekItemsQuestions(){
        ApiInterface apiInterface = ApiClient.getRetrofit().create(ApiInterface.class);
        Call<QuestionsModel> call = apiInterface.getWeekQuestions();
        call.enqueue(new Callback<QuestionsModel>() {
            @Override
            public void onResponse(Call<QuestionsModel> call, Response<QuestionsModel> response) {
                if (response.body() != null && response.isSuccessful()){
                    if (response.code() == 200){
                        weekItemsQuestionsList.setValue(response.body().getItemsModel());
                    }else {
                        Log.e("Error: ", response.code() + " " + response.message());
                    }
                }
            }

            @Override
            public void onFailure(Call<QuestionsModel> call, Throwable t) {
                t.printStackTrace();
            }
        });
    }
}
