package com.example.solonez.stackoverflowviewer.api;


import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

/**
 * Created by Jakub Solecki on 2018-06-13.
 */
public interface ApiInterface {

    @GET("questions?order=desc&sort=hot&site=stackoverflow")
    Call<QuestionsModel> getHottestQuestions();

    @GET("questions?order=desc&sort=week&site=stackoverflow")
    Call<QuestionsModel> getWeekQuestions();

    @GET("search")
    Call<QuestionsModel> getSearchQuestions(@Query("order") String order,
                                            @Query("sort") String activity,
                                            @Query("intitle") String intitle,
                                            @Query("site") String site);
}
