package com.example.solonez.stackoverflowviewer.main.week_questions;

import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import com.example.solonez.stackoverflowviewer.api.ItemsModel;
import com.example.solonez.stackoverflowviewer.ui.ActionBarActivity;
import com.example.solonez.stackoverflowviewer.R;
import java.util.List;
import butterknife.BindView;
import butterknife.ButterKnife;

public class WeekQuestionsActivity extends ActionBarActivity {

    @BindView(R.id.week_recycler_view) RecyclerView recyclerView;
    private RecyclerView.Adapter adapter;
    private WeekQuestionsViewModel weekQuestionsViewModel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_week_questions);
        ButterKnife.bind(this);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(WeekQuestionsActivity.this);
        recyclerView.setLayoutManager(layoutManager);
        weekQuestionsViewModel = ViewModelProviders.of(this).get(WeekQuestionsViewModel.class);
        weekQuestionsViewModel.getWeekItemsQuestions();
        adapter = new WeekQuestionsAdapter((weekQuestionsViewModel.getWeekItemsQuestionsList()));
        recyclerView.setAdapter(adapter);
        initializeObserver();
    }

    @Override
    protected boolean getBackButtonEnabled() {
        return true;
    }

    @Override
    protected String getActionBarTitle() {
        return getString(R.string.week_questions);
    }

    private void initializeObserver(){
        final Observer<List<ItemsModel>> questions = new Observer<List<ItemsModel>>(){

            @Override
            public void onChanged(@Nullable List<ItemsModel> itemsModelList) {
                adapter.notifyDataSetChanged();
            }
        };
        weekQuestionsViewModel.getWeekItemsQuestionsList().observe(this, questions);
    }
}
